package com.zuitt.example;

// Child class of Animal Class
    //"extend" keyword used to inherit the properties and methods of the parent class.
public class Dog extends Animal{
    //properties
    private  String breed;

    //constructor
    public Dog(){
        //this will
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    public String getBreed(){
        return this.breed;
    }
}
